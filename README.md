# BackupIt

PowerShell scripting software that allows scheduling and executing backup operations with WinRAR.


[[_TOC_]]


## 1. Requirements

* ***Windows 10 or 11 (64-bit version)*** (including PowerShell 5.1)
* ***WinRAR 6.x (64-bit version)***


## 2. Installation

* Run PowerShell as administrator, execute the following command and exit (to be done once and for all):
  ```
  Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope LocalMachine -Force
  ```
* Create the directory `C:\Program Files\BackupIt\` and copy the following script files to this directory:
  * *Backup.ps1*
  * *CheckConfigFile.ps1*
  * *CleanTaskScheduler.ps1*
  * *ConfigureTaskScheduler.ps1*
  * *Log.ps1*
  * *RunBackupTask.ps1*
* Create the directory `C:\Users\%USERNAME%\AppData\Roaming\BackupIt\` and copy the file *config.xml* to this directory.
* Copy the following shortcuts somewhere:
  * *Clean Task Scheduler*
  * *Configure Task Scheduler*
  * *Run All Backup Tasks*
  * *Run Backup Task 'Example'*


## 3. Configuration

* In the file `C:\Users\%USERNAME%\AppData\Roaming\BackupIt\config.xml`, follow the comments.
* As many shortcuts *Run Backup Task '...'* can be created as there are backup items configured in the file `config.xml`.
  In the shortcut *Run Backup Task 'Example'*, replace "Example" with the name of the item to be configured:
  * in the field "Target" of the window "Properties" (right click / Properties on the shortcut)
  * in the title (to be aesthetically consistent)


## 4. Use

* By definition, all automatic backups configured in the file `config.xml` are performed automatically.
* **Important:** When the value of a field related to the schedule is modified in the file `config.xml` (day, time, status, automatic), execute the shortcut *Configure Task Scheduler* (administrator rights required) to make it taken into consideration.
* To manually perform all backups configured in the file `config.xml`, execute the shortcut *Run All Backup Tasks*.
* To manually perform a backup configured in the file `config.xml`, execute the corresponding shortcut *Run Backup Task '...'*.
* To know the history of operations, check the log file `C:\Users\%USERNAME%\AppData\Roaming\BackupIt\backupit.log`.
* To stop all backup configurations, execute the shortcut *Clean Task Scheduler* (administrator rights required).


## 5. Flowsheets

```mermaid
graph LR
    subgraph WINDOWS TASK SCHEDULER
    TS{Task Scheduler}
    end
    CTS1-S[ConfigureTaskScheduler.ps1] --> TS
    RBT-S[RunBackupTask.ps1] --> TS
    CTS2-S[CleanTaskScheduler.ps1] --> TS
    TS --> B-S[Backup.ps1]
```
```mermaid
graph LR
    subgraph CONFIGURE TASK SCHEDULER script
    CTS1-S[ConfigureTaskScheduler.ps1]
    end
    CTS1-S --> L-S[Log.ps1]
    CTS1-S --> CCF-S[CheckConfigFile.ps1]
    CTS1-S --> C-F[config.xml]
    C-F --> CTS1-S
    CTS1-S --> TS{Task Scheduler}
```
```mermaid
graph LR
    subgraph RUN BACKUP TASK script
    RBT-S[RunBackupTask.ps1]
    end
    RBT-S --> TS{Task Scheduler}
```
```mermaid
graph LR
    subgraph BACKUP script
    B-S[Backup.ps1] --> B((Backup))
    end
    TS{Task Scheduler} --> B-S
    B-S --> L-S[Log.ps1]
    B-S --> CCF-S[CheckConfigFile.ps1]
    B-S --> C-F[config.xml]
    C-F --> B-S
```
```mermaid
graph LR
    subgraph LOG script
    L-S[Log.ps1] --> B-L[backupit.log]
    end
    CTS1-S[ConfigureTaskScheduler.ps1] --> L-S
    CCF-S[CheckConfigFile.ps1] --> L-S
    B-S[Backup.ps1] --> L-S
    CTS2-S[CleanTaskScheduler.ps1] --> L-S
```
```mermaid
graph LR
    subgraph CHECK CONFIG FILE script
    CCF-S[CheckConfigFile.ps1]
    end
    CTS1-S[ConfigureTaskScheduler.ps1] --> CCF-S
    B-S[Backup.ps1] --> CCF-S
    CCF-S --> L-S[Log.ps1]
    CCF-S --> C-F[config.xml]
    C-F --> CCF-S
```
```mermaid
graph LR
    subgraph CLEAN TASK SCHEDULER script
    CTS2-S[CleanTaskScheduler.ps1]
    end
    CTS2-S --> TS{Task Scheduler}
    CTS2-S --> L-S[Log.ps1]
```


## 6. Possible future features

* Graphical user interface
* Automated installation procedure
