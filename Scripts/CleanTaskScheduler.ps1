﻿# Set the scripts path.
$scriptsPath = "C:\Program Files\BackupIt\"

# Set the log script path.
$logScriptPath = $scriptsPath + "Log.ps1"


If (Get-ScheduledTask | Select-String -Pattern BackupIt)
{
  # Start a new section in the log file.
  & $logScriptPath -comment "----------------------------------------------------------------------------------------------------" 
  Unregister-ScheduledTask -TaskPath "\BackupIt\" -Confirm:$False
  # Stop the section in the log file.
  & $logScriptPath -comment "Task scheduler configuration cleaned"
  & $logScriptPath -comment "----------------------------------------------------------------------------------------------------"
  Return
}
