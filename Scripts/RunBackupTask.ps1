﻿param (
  [string] $itemName
)


function RunTask {
  param (
    $taskName
  )
  # Check no other task is running.
  While ((Get-ScheduledTask -TaskPath "\BackupIt\").State -contains "Running")
  {
    Start-Sleep -Seconds 1
  }
  # Run the task.
  Start-ScheduledTask -TaskPath "\BackupIt\" -TaskName $taskName
}


If (!$itemName) # Case #1: all the items.
{
  # Run all the tasks one after the other.
  foreach ($itemName in (Get-ScheduledTask -TaskPath "\BackupIt\").TaskName)
  {
    RunTask -taskName $itemName
  }
}else{ # Case #2: one item.
  # Check the task exists, otherwise exit.
  If (!((Get-ScheduledTask -TaskPath "\BackupIt\").TaskName -match ("^" + $itemName.Trim() + "$"))) {Return}
  # Run the task.
  RunTask -taskName $itemName.Trim()
}
