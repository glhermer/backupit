﻿param (
  [string] $itemName
)


# Set the scripts path.
$scriptsPath = "C:\Program Files\BackupIt\"

# Set the log script path.
$logScriptPath = $scriptsPath + "Log.ps1"

# Set the config file path.
$configFilePath = "C:\Users\$Env:username\AppData\Roaming\BackupIt\config.xml"


function ReturnTrue {
  & $logScriptPath -comment "Config file check OK"
  Return $True
}

function ReturnFalse {
  param (
    [string] $description
  )
  & $logScriptPath -comment "Config file check KO: $description"
  Return $False
}

function CheckItem {
  param (
    [string] $itemName
  )

  [xml]$xml = Get-Content -Path $configFilePath -Encoding "utf8"
  $items = $xml.BackupIt.Items
  $item = $items.item | Where-Object {$_.Name -match "^\s*$itemName\s*$"}

  [int] $count = 0
  [string] $description = ""
  If (!$item.itemToBeBackuped)
  {
    $description = "$description itemToBeBackuped"
    $count++
  }
  If (!$item.directoryToDropTheArchive)
  {
    $description = "$description directoryToDropTheArchive"
    $count++
  }
  If (!$item.compressionLevel)
  {
    $description = "$description compressionLevel"
    $count++
  }
  If (!$item.maxNumberOfArchivesToBeKept)
  {
    $description = "$description maxNumberOfArchivesToBeKept"
    $count++
  }
  If (!$item.day)
  {
    $description = "$description day"
    $count++
  }
  If (!$item.time)
  {
    $description = "$description time"
    $count++
  }
  If (!$item.status)
  {
    $description = "$description status"
    $count++
  }
  If (!$item.automatic)
  {
    $description = "$description automatic"
    $count++
  }
  If ($count -gt 0)
  {
    $description = "item node(s) missing ($description )"
    Return $description
  }else{
    [int] $count = 0
    [string] $description = ""
    If (!(Test-Path -Path $item.itemToBeBackuped.Trim()))
    {
      $description = "$description itemToBeBackuped"
      $count++
    }
    If (!(Test-Path -Path $item.directoryToDropTheArchive.Trim()))
    {
      $description = "$description directoryToDropTheArchive"
      $count++
    }
    If (!("0","1","2","3","4","5" -contains $item.compressionLevel.Trim()))
    {
      $description = "$description compressionLevel"
      $count++
    }
    If (!($item.maxNumberOfArchivesToBeKept.Trim() -match "^[0-9]+$"))
    {
      $description = "$description maxNumberOfArchivesToBeKept"
      $count++
    }
    If (!("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday" -contains $item.day.Trim()))
    {
      $description = "$description day"
      $count++
    }
    If (!($item.time.Trim() -match "^([0-9]|[0-1][0-9]|2[0-3]):[0-5][0-9]$"))
    {
      $description = "$description time"
      $count++
    }
    If (!(($item.status.Trim() -eq "True") -or ($item.status.Trim() -eq "False")))
    {
      $description = "$description status"
      $count++
    }
    If (!(($item.automatic.Trim() -eq "True") -or ($item.automatic.Trim() -eq "False")))
    {
      $description = "$description automatic"
      $count++
    }
    If ($count -gt 0)
    {
      $description = "item parameter(s) invalid ($description )"
      Return $description
    }
  }
}


# Check the config file exists.
If (!(Test-Path -Path $configFilePath))
{
  ReturnFalse -description "config file missing"
}else{
  [xml]$xml = Get-Content -Path $configFilePath -Encoding "utf8"
  # Check the config file structure is valid.
  If ($xml -eq $null)
  {
    ReturnFalse -description "config file structure invalid"
  }else{
    # Check the root node exists.
    If (!$xml.BackupIt)
    {
      ReturnFalse -description "root node missing"
    }else{
      $BackupIt = $xml.BackupIt
      # Check the WinRAR executable node exists.
      If (!$BackupIt.WinRAR.path.Trim())
      {
        ReturnFalse -description "WinRAR executable path missing"
      }else{
        # Check the WinRAR executable node is valid.
        If (!(Test-Path -Path $BackupIt.WinRAR.path.Trim()))
        {
          ReturnFalse -description "WinRAR executable path invalid"
        }else{
          # Check the items node exists.
          If (!$BackupIt.Items)
          {
            ReturnFalse -description "items node missing"
          }else{
            # Check no item has the same name as another one.
            $itemNameList = @()
            foreach ($item in $BackupIt.Items.item)
            {
              $itemNameList += $item.name.Trim()
            }
            If ((Compare-Object $itemNameList ($itemNameList | Get-Unique)) -ne $null)
            {
              ReturnFalse -description "At least two items have the same name."
            }else{
              # Check the item nodes exist and their values are valid.
              If (!$itemName) # Case #1: all the items.
              {
                foreach ($item in $BackupIt.Items.item)
                {
                  [string] $description = CheckItem -itemName $item.name
                  If ($description -ne $null)
                  {
                    $description = $item.name.Trim() + " - $description"
                    ReturnFalse -description $description
                    break
                    }
                }
                If ($description -eq $null)
                {
                  ReturnTrue
                }
              }else{ # Case #2: one item.
                If (!($BackupIt.Items.item | Where-Object {$_.Name -match "^\s*$itemName\s*$"}))
                {
                  ReturnFalse -description "item node missing"
                }else{
                  [string] $description = CheckItem -itemName $itemName
                  If ($description -ne $null)
                  {
                    ReturnFalse -description $description
                  }else{
                    ReturnTrue
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
