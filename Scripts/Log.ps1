﻿param (
  [string] $comment
)


# Set the log file path.
$logFilePath = "C:\Users\$Env:username\AppData\Roaming\BackupIt\backupit.log"


# Check the log file exists, otherwise create it.
If (!$logFilePath) {New-Item -ItemType File -Name $logFilePath -Force}

# Check a comment is provided, otherwise exit.
If (!$comment) {Return}


# Log the comment.
Add-Content -Path $logFilePath -Encoding "utf8" -Value ($(Get-Date -Format "yyyy-MM-dd_HH:mm:ss") + "  " + $comment)
