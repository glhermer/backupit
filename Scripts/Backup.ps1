﻿param (
  [string] $itemName
)


# Set the scripts path.
$scriptsPath = "C:\Program Files\BackupIt\"

# Set the log script path.
$logScriptPath = $scriptsPath + "Log.ps1"

# Set the check config file script path.
$checkConfigFileScriptPath = $scriptsPath + "CheckConfigFile.ps1"

# Set the config file path.
$configFilePath = "C:\Users\$Env:username\AppData\Roaming\BackupIt\config.xml"


# Check an item is provided, otherwise exit.
If (!$itemName) {Return}


# Start a new section in the log file.
& $logScriptPath -comment "----------------------------------------------------------------------------------------------------"


# Check the config file is valid, otherwise exit.
If (!(& $checkConfigFileScriptPath -itemName $itemName.Trim()))
{
  # Stop the section in the log file.
  & $logScriptPath -comment "----------------------------------------------------------------------------------------------------"
  Return
}

# Load the config file.
[xml]$xml = Get-Content -Path $configFilePath -Encoding "utf8"
$BackupIt = $xml.BackupIt

# Get the WinRAR executable path.
$winrarExecPath = $BackupIt.WinRAR.path

# Get the backup item.
$item = $BackupIt.Items.item | Where-Object -Property Name -eq "$itemName"


# Case 1: the item is a directory.
If ((Get-Item -Path $item.itemToBeBackuped.Trim()) -Is [System.IO.DirectoryInfo])
{
  # Get the full path of the parent directory hosting the directory to be backuped.
  $parentDirectory_fullName = (Get-Item -Path $item.itemToBeBackuped.Trim()).Parent.FullName
}

# Case 2: the item is a file.
If ((Get-Item -Path $item.itemToBeBackuped.Trim()) -Is [System.IO.FileInfo])
{
  # Get the full path of the parent directory hosting the file to be backuped.
  $parentDirectory_fullName = (Get-Item -Path $item.itemToBeBackuped.Trim()).Directory.FullName
}

# Get the name of the item to be backuped.
$item_name = (Get-Item -Path $item.itemToBeBackuped.Trim()).Name

# Get the full path of the archive that will be created.
$backupFile_fullName = $item.directoryToDropTheArchive.Trim() + $item_name + "_" + $(Get-Date -Format "yyyy-MM-dd") + ".rar"


# Delete the archive if it already exists.
If (Test-Path -Path $backupFile_fullName)
{
  Remove-Item -Path $backupFile_fullName
  & $logScriptPath -comment "Former archive ""$backupFile_fullName"" deleted"
}


# Perform the backup (WinRAR documentation: https://www.winrar-france.fr/aide_winrar/).
If ($item.password -ne "")
{
  $argList = @("a", "-ma", ('"-m' + $item.compressionLevel.Trim() + '"'), ('"-hp' + $item.password + '"'), ('"' + $backupFile_fullName + '"'), ('"' + $item_name + '"'))
}else{
  $argList = @("a", "-ma", ('"-m' + $item.compressionLevel.Trim() + '"'), ('"' + $backupFile_fullName + '"'), ('"' + $item_name + '"'))
}
& $logScriptPath -comment "Backup operation started"
Start-Process -FilePath $winrarExecPath -WorkingDirectory $parentDirectory_fullName -ArgumentList $argList -NoNewWindow -Wait
& $logScriptPath -comment "Archive ""$backupFile_fullName"" created"
& $logScriptPath -comment "Backup operation completed"


# Delete the old archives.
function ListArchives {
  (Get-ChildItem -Path $item.directoryToDropTheArchive.Trim() -File | Where-Object {$_.Name -match "^$item_name" -and $_.Name -match "rar$"}).FullName
}
$archivesList = ListArchives
While ($archivesList.Count -gt $item.maxNumberOfArchivesToBeKept.Trim())
{
  Remove-Item -Path $archivesList[0]
  & $logScriptPath -comment "Old archive ""$($archivesList[0])"" deleted"
  $archivesList = ListArchives
}


# Stop the section in the log file.
& $logScriptPath -comment "----------------------------------------------------------------------------------------------------"
