﻿# Set the scripts path.
$scriptsPath = "C:\Program Files\BackupIt\"

# Set the log script path.
$logScriptPath = $scriptsPath + "Log.ps1"

# Set the check config file script path.
$checkConfigFileScriptPath = $scriptsPath + "CheckConfigFile.ps1"

# Set the config file path.
$configFilePath = "C:\Users\$Env:username\AppData\Roaming\BackupIt\config.xml"

# Set the backup script name.
$backupScriptName = "Backup.ps1"


# Start a new section in the log file.
& $logScriptPath -comment "----------------------------------------------------------------------------------------------------"

# Check the config file is valid, otherwise exit.
If (!(& $checkConfigFileScriptPath))
{
  # Stop the section in the log file.
  & $logScriptPath -comment "Task scheduler configuration not updated"
  & $logScriptPath -comment "----------------------------------------------------------------------------------------------------"
  Return
}

# Load the config file.
[xml]$xml = Get-Content -Path $configFilePath -Encoding "utf8"
$items = $xml.BackupIt.Items


function SetScheduledTask {
  param (
    $item
  )

  # Create the action.
  $Action= New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument "`"./$($backupScriptName) '$($item.name)'`"" -WorkingDirectory $scriptsPath

  # Specify some settings.
  $Principal = New-ScheduledTaskPrincipal -UserId "$(whoami)" -LogonType S4U
  $Settings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -Compatibility Win8

  # Create the trigger.
  $Trigger= New-ScheduledTaskTrigger -At $item.time.Trim() -DaysOfWeek $item.day.Trim() –Weekly
  
  # Create the task.
  If ($item.automatic.Trim() -eq "True")
  {
    $Task = New-ScheduledTask -Action $Action -Principal $Principal -Settings $Settings -Trigger $Trigger
  }else{
    $Task = New-ScheduledTask -Action $Action -Principal $Principal -Settings $Settings
  }

  # Add the task to the scheduler.
  Register-ScheduledTask -TaskPath "\BackupIt\" -TaskName $item.name.Trim() -InputObject $Task -Force

}

function UnsetScheduledTask {
  param (
    $item
  )
  Unregister-ScheduledTask -TaskPath "\BackupIt\" -TaskName $item.name.Trim() -Confirm:$False
}


# Clean all existing tasks in the scheduler.
If (Get-ScheduledTask | Select-String -Pattern BackupIt)
{
  Unregister-ScheduledTask -TaskPath "\BackupIt\" -Confirm:$False
}

# Configure the tasks in the scheduler.
foreach ($item in $items.item)
{
  If ($item.status.Trim() -eq "True")
  {
    SetScheduledTask $item
  }
  If (($item.status.Trim() -eq "False") -and ((Get-ScheduledTask -TaskPath "\BackupIt\").TaskName -match ("^" + $item.name.Trim() + "$")))
  {
    UnsetScheduledTask $item
  }
 }

# Stop the section in the log file.
& $logScriptPath -comment "Task scheduler configuration updated"
& $logScriptPath -comment "----------------------------------------------------------------------------------------------------"
